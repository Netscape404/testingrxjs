import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExerciceRoutingModule } from './exercice-routing.module';
import { Exo1Component } from './containers/exo1/exo1.component';
import { Exo2Component } from './containers/exo2/exo2.component';
import { SimpleCounterComponent } from './components/simple-counter/simple-counter.component';

@NgModule({
  declarations: [Exo1Component, Exo2Component, SimpleCounterComponent],
  imports: [
    CommonModule,
    ExerciceRoutingModule
  ]
})
export class ExerciceModule { }
