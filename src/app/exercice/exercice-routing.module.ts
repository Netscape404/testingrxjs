import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Exo1Component } from './containers/exo1/exo1.component';
import { Exo2Component } from './containers/exo2/exo2.component';

const routes: Routes = [
  { path: 'exo1', component: Exo1Component },
  { path: 'exo2', component: Exo2Component },
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExerciceRoutingModule { }
