import {Component, Input} from '@angular/core';
import {interval} from 'rxjs';
import {endWith, map, takeWhile, tap} from 'rxjs/operators';

@Component({
  selector: 'app-simple-counter',
  templateUrl: './simple-counter.component.html',
  styleUrls: ['./simple-counter.component.sass']
})
export class SimpleCounterComponent {
  @Input() startAt;
  @Input() endAt;
  @Input() step;
  @Input() delay;
  @Input() digits;
  currentValue: number;

  reset() {
    interval(this.delay).pipe(
      map(val => this.startAt + val * this.step),
      takeWhile(val => val < this.endAt),
      endWith(this.endAt),
    ).subscribe(val => this.currentValue = val);
  }
}
